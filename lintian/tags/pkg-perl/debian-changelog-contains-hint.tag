Tag: debian-changelog-contains-hint
Severity: warning
Check: pkg-perl/changelog
Explanation: debian/changelog contains a keyword like PROBLEM, TODO, NOTE,
 WARNING, FIXME, QUESTION, WAITS-FOR, IGNORE-VERSION. This can mean that
 there is still an issue to resolve, or that the note can be removed before
 uploading.
