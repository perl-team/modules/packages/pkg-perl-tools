Tag: build-deps-can-be-annotated-nocheck
Severity: info
Check: pkg-perl/nocheck
Explanation: To support bootstrapping, build dependencies that are only
 needed for the build tests should be annotated with <!nocheck>. This package
 does not contain any such annotation, while listing a libtest-* package in
 its build dependencies. Please add <!nocheck> as appropriate.
