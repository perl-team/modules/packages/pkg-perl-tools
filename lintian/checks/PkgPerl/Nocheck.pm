# pkg-perl/nocheck -- lintian check script (rewrite) -*- perl -*-
#
# based on lintian's fields/package-relations
#
# Copyright © 2004 Marc Brockschmidt
# Copyright © 2019-2020 Chris Lamb <lamby@debian.org>
# Copyright © 2021 Florian Schlichting <fsfs@debian.org>
#
# Parts of the code were taken from the old check script, which
# was Copyright © 1998 Richard Braakman (also licensed under the
# GPL 2 or higher)
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, you can find it on the World Wide
# Web at http://www.gnu.org/copyleft/gpl.html, or write to the Free
# Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston,
# MA 02110-1301, USA.

package Lintian::Check::PkgPerl::Nocheck;

use v5.20;
use warnings;
use utf8;
use autodie;

use Moo;
use namespace::clean;

with 'Lintian::Check';

sub source {
    my ($self) = @_;

    my $processable = $self->processable;

    # Only for pkg-perl packages
    return
        unless $processable->fields->value('Maintainer')
        =~ /pkg-perl-maintainers\@lists\.alioth\.debian\.org/sm;


    # look at Build-Depends, Build-Depends-Indep
    my $data = $processable->fields->unfolded_value('Build-Depends');
    if ($processable->fields->declares('Build-Depends-Indep')) {
        $data .= ',' . $processable->fields->unfolded_value('Build-Depends-Indep');
    }

    my ($seen_nocheck, $seen_libtest);
    for my $dep (split /\s*[,]\s*/, $data) {
        $seen_nocheck = 1 if $dep =~ /!nocheck/;
        $seen_libtest = 1 if $dep =~ /libtest\S*-perl/;
    }

    $self->hint('build-deps-can-be-annotated-nocheck') if $seen_libtest && !$seen_nocheck;
}

1;

# Local Variables:
# indent-tabs-mode: nil
# cperl-indent-level: 4
# End:
# vim: syntax=perl sw=4 sts=4 sr et
