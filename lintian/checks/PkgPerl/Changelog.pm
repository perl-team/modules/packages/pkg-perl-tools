# pkg-perl/changelog -- lintian check script for debian/changelog in pkg-perl -*- perl -*-

# based on checks/debian/changelog.pm in lintian, where it might be
# incorporated eventually

# Copyright © 1998 Christian Schwarz
# Copyright © 2019-2020 Chris Lamb <lamby@debian.org>
# Copyright © 2020 gregor herrmann <gregoa@debian.org>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, you can find it on the World Wide
# Web at http://www.gnu.org/copyleft/gpl.html, or write to the Free
# Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston,
# MA 02110-1301, USA.

package Lintian::Check::PkgPerl::Changelog;

use v5.20;
use warnings;
use utf8;
use autodie;

use Path::Tiny;

use Moo;
use namespace::clean;

with 'Lintian::Check';

sub source {
    my ($self) = @_;

    my $processable = $self->processable;

    # Only for pkg-perl packages
    return
        unless $processable->fields->value('Maintainer')
        =~ /pkg-perl-maintainers\@lists\.alioth\.debian\.org/sm;

    my $changelog = $processable->changelog;
    return unless defined $changelog;

    my @entries = @{ $changelog->entries };
    return unless @entries;

    my $latest_entry = $entries[0];

    # only if not UNRELEASED
    return if $latest_entry->Distribution eq 'UNRELEASED';

    $self->hint( 'debian-changelog-contains-hint', $1 )
        if $latest_entry->Changes
        =~ /^\s+(WAITS-FOR|IGNORE-VERSION|PROBLEM|TODO|NOTE|WARNING|FIXME|QUESTION):/;
}

1;

# Local Variables:
# indent-tabs-mode: nil
# cperl-indent-level: 4
# End:
# vim: syntax=perl sw=4 sts=4 sr et
