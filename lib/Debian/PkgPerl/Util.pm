package Debian::PkgPerl::Util;

use strict;
use warnings;
use Carp;

=head1 NAME

Debian::PkgPerl::Util - utility functions for PkgPerl tools

=head1 SYNOPSIS

 use Debian::PkgPerl::Util;
 ...
 $dep = "$dep <!nocheck>"
     unless Debian::PkgPerl::Util->probable_build_dependency($pkg);

 $cachefile = Debian::PkgPerl::Util->download_and_cache_file(
     "https://example.com/foo.txt",
     "bla.txt",
     86400
 );

=head1 DESCRIPTION

B<Debian::PkgPerl::Util> contains some utility functions for PkgPerl tools.

=head1 METHODS

=over 4

=item B<probable_build_dependency>(I<package name>)

Returns true if the package name is likely to be used for building, and
not only at runtime or only for testing.

=cut

sub probable_build_dependency {
    my ($class, $pkg) = @_;

    return $pkg =~ /-dev(?::.+)?$/ || $pkg =~ /^(?x:
        .^ # a placeholder to help with aesthetics
        | autoconf
        | automake
        | bash-completion
        | bison
        | cdbs
        | chrpath
        | debhelper
        | dh-
        | flex
        | gnulib
        | help2man
        | install-info
        | libalien-
        | libcanary-stability-perl
        | libconfig-autoconf-perl
        | libcrypt-openssl-guess-perl
        | libdevel-checklib-perl
        | libdevel-mat-dumper-perl
        | libdist-build-perl
        | libextutils-
        | libffi-checklib-perl
        | libfile-sharedir-
        | libmodule-build-
        | libmodule-install-
        | libparse-recdescent-perl
        | libparse-yapp-perl
        | libpkgconfig-perl
        | libtool
        | libxs-parse-sublike-perl
        | llvm
        | perl
        | pkg-config
        | pkgconf
        | po-debconf
        | po4a
        | rename
        | swig
        | ucf
        )/;
}

=pod

=item B<download_and_cache_file>(I<URL>, I<local filename>, I<expiration time in seconds>)

Helper function to download a file from I<URL> and save it in
F<$ENV{XDG_CACHE_HOME}/pkg-perl-tools/> (or F<$ENV{HOME}/.cache/pkg-perl-tools/>)
as I<filename>, unless the file already exists and is younger than
I<expiration time> in seconds.

Returns the full filename as a string.

=back

=cut

sub download_and_cache_file {
    my ( $class, $url, $filename, $expiration_time ) = @_;

    require HTTP::Tiny;
    HTTP::Tiny->import;
    require Path::Tiny;
    Path::Tiny->import;

    my $cache     = $ENV{XDG_CACHE_HOME} || $ENV{HOME} . '/.cache';
    my $cache_dir = path( $cache, 'pkg-perl-tools' );
    $cache_dir->mkdir;
    my $cache_file = path( $cache_dir, $filename );

    if ( -e $cache_file ) {
        my $age = time - $cache_file->stat->ctime;
        if ( $age <= $expiration_time ) {
            return $cache_file;
        } else {
            $cache_file->remove;
        }
    }

    my $response = HTTP::Tiny->new->mirror( $url, $cache_file );
    if ( !$response->{success} ) {
        croak "Downloading '$url' failed: $response->{status} $response->{reason}";
    }
    return $cache_file;
}

1;
__END__

=head1 COPYRIGHT AND LICENSE

=over 4

=item Copyright 2022-2023 gregor herrmann L<gregoa@debian.org>

=item Copyright 2022 Damyan Ivanov L<dmn@debian.org>

=back

This program is free software; you can redistribute it and/or modify it
under the terms of either: the GNU General Public License as published
by the Free Software Foundation; or the Artistic License.

See L<http://dev.perl.org/licenses/> for more information.

=cut
