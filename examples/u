#!/bin/bash

# Copyright 2019-2024, gregor herrmann <gregoa@debian.org>
# Released under the WTFPL

# initialize begin
for f in ~/.dpt.conf ~/.config/dpt.conf; do
	if [ -r "$f" ]; then
		. "$f"
	fi
done
. "${DPT__SCRIPTS:-/usr/share/pkg-perl-tools}/lib/dpt-lib.sh"
# initialize end


[ -n "$PKG" ] && [ -n "$VER" ] && [ -n "$DIST" ] || \
	die "Source or Version or Distribution not set."

NEW=
while getopts n opt; do
	case $opt in
		n) NEW="--new";;
		*) ;;
	esac
done
shift $((OPTIND -1))

if git commit --dry-run -a > /dev/null ; then
	header "Release package"
	git add debian/changelog
	git commit -S -m "releasing package $PKG version $VER into distribution $DIST" -m 'Gbp-Dch: Ignore'
fi

if git commit --dry-run -a > /dev/null ; then
	die "git tree dirty …"
fi

header "dgit push-source"
if promptyesno "dgit --gbp $NEW push-source?"; then
	echo
	dgit --gbp $NEW push-source
	if [ $? -ne 0 ]; then
		OPTION=--overwrite
		if is_installed dgit 11.7; then
			OPTION=--trust-changelog
		fi
		if promptyesno "dgit --gbp $NEW $OPTION push-source?"; then
			echo
			dgit --gbp $NEW $OPTION push-source
			if [ $? -ne 0 ]; then
				echo
				die "'dgit --gbp $OPTION push-source' failed"
			fi
		fi
	fi
	if [ $? -eq 0 ]; then
		header "dpt push"
		if promptyesno "dpt push?"; then
			echo
			dpt push
		fi

		EVER=$(echo "$VER" | sed -e 's/^[0-9]\+://')
		BUILDINFO=$(ls -rt1 ../{build-area/,}${PKG}_${EVER}_*.buildinfo 2>/dev/null | tail -n1)
		if [ -f "$BUILDINFO" ]; then
			header ".buildinfo"
			if promptyesno "Upload .buildinfo to buildinfo.debian.net?"; then
				echo
				if ! grep -q "BEGIN PGP SIGNATURE" "$BUILDINFO"; then
					debsign --no-re-sign "$BUILDINFO"
				fi
				curl -X PUT --max-time 30 --data-binary @${BUILDINFO} https://buildinfo.debian.net/api/submit
			fi
		fi
	fi
	# cf. #1056103; fixed in 11.6
	rm -rf .git/dgit/unpack
fi
