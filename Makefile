#!/usr/bin/make
# vim: noet ft=make


.PHONY: all
all:

PREFIX = usr
SCRIPTS := $(filter-out lib,$(notdir $(wildcard scripts/*)))
SCRIPT_LIBS := $(notdir $(wildcard scripts/lib/*))
BINS := $(notdir $(wildcard bin/*))
BIN_MANUALS := $(addsuffix .1,$(BINS))
LIBS := $(notdir $(wildcard lib/Debian/PkgPerl/*))
# Using // instead of :: is a hack to find dependencies later
LIB_MANUALS := $(addprefix Debian//PkgPerl//,$(patsubst %.pm,%.3pm,$(LIBS)))
CONF_MANUALS := $(patsubst %.pod,%,$(notdir $(wildcard man5/*)))
LINTIAN_CHECKS := $(notdir $(wildcard lintian/checks/PkgPerl/*))
LINTIAN_PROFILES := $(notdir $(wildcard lintian/profiles/pkg-perl/*))
LINTIAN_TAGS := $(notdir $(wildcard lintian/tags/pkg-perl/*))
AUTOPKGTEST_SCRIPTS := $(notdir $(wildcard autopkgtest/scripts/runner))
AUTOPKGTEST_SCRIPTS_BUILDDEPS := $(notdir $(wildcard autopkgtest/scripts/build-deps.d/*))
AUTOPKGTEST_SCRIPTS_RUNTIMEDEPS := $(notdir $(wildcard autopkgtest/scripts/runtime-deps.d/*))
AUTOPKGTEST_SCRIPTS_RUNTIMEDEPSRECOMMENDS := $(notdir $(wildcard autopkgtest/scripts/runtime-deps-and-recommends.d/*))
AUTOPKGTEST_SCRIPTS_HEAVYDEPS := $(notdir $(wildcard autopkgtest/scripts/heavy-deps.d/*))
ZSH_COMPLETIONS := $(notdir $(wildcard shell/zsh/*))
VERSION := $(shell dpkg-parsechangelog --show-field Version)

MAN_DIR := $(PREFIX)/share/man/man1
MAN3_DIR := $(PREFIX)/share/man/man3
MAN5_DIR := $(PREFIX)/share/man/man5

INSTALLED_SCRIPTS := $(addprefix $(DESTDIR)/$(PREFIX)/share/pkg-perl-tools/,$(SCRIPTS))
INSTALLED_SCRIPT_MANUALS := $(addprefix $(DESTDIR)/$(MAN_DIR)/dpt-,$(addsuffix .1,$(filter-out posix-lib.sh,$(SCRIPTS))))
INSTALLED_SCRIPT_ALIAS_MANUALS := $(addprefix $(DESTDIR)/$(MAN_DIR)/dpt-,$(addsuffix .1,co))
INSTALLED_SCRIPT_LIBS := $(addprefix $(DESTDIR)/$(PREFIX)/share/pkg-perl-tools/lib/,$(SCRIPT_LIBS))
INSTALLED_BINS := $(addprefix $(DESTDIR)/$(PREFIX)/bin/,$(BINS))
INSTALLED_BIN_MANUALS := $(addprefix $(DESTDIR)/$(MAN_DIR)/,$(BIN_MANUALS))
INSTALLED_LIBS := $(addprefix $(DESTDIR)/$(PREFIX)/share/perl5/Debian/PkgPerl/,$(LIBS))
INSTALLED_LIB_MANUALS := $(addprefix $(DESTDIR)/$(MAN3_DIR)/,$(LIB_MANUALS))
INSTALLED_LINTIAN_CHECKS := $(addprefix $(DESTDIR)/$(PREFIX)/share/lintian/lib/Lintian/Check/PkgPerl/, $(LINTIAN_CHECKS))
INSTALLED_LINTIAN_PROFILES := $(addprefix $(DESTDIR)/$(PREFIX)/share/lintian/profiles/pkg-perl/, $(LINTIAN_PROFILES))
INSTALLED_LINTIAN_TAGS := $(addprefix $(DESTDIR)/$(PREFIX)/share/lintian/tags/pkg-perl/, $(LINTIAN_TAGS))
INSTALLED_AUTOPKGTEST_SCRIPTS:= $(addprefix $(DESTDIR)/$(PREFIX)/share/pkg-perl-autopkgtest/, $(AUTOPKGTEST_SCRIPTS))
INSTALLED_AUTOPKGTEST_SCRIPTS_BUILDDEPS:= $(addprefix $(DESTDIR)/$(PREFIX)/share/pkg-perl-autopkgtest/build-deps.d/, $(AUTOPKGTEST_SCRIPTS_BUILDDEPS))
INSTALLED_AUTOPKGTEST_SCRIPTS_RUNTIMEDEPS:= $(addprefix $(DESTDIR)/$(PREFIX)/share/pkg-perl-autopkgtest/runtime-deps.d/, $(AUTOPKGTEST_SCRIPTS_RUNTIMEDEPS))
INSTALLED_AUTOPKGTEST_SCRIPTS_RUNTIMEDEPSRECOMMENDS:= $(addprefix $(DESTDIR)/$(PREFIX)/share/pkg-perl-autopkgtest/runtime-deps-and-recommends.d/, $(AUTOPKGTEST_SCRIPTS_RUNTIMEDEPSRECOMMENDS))
INSTALLED_AUTOPKGTEST_SCRIPTS_HEAVYDEPS:= $(addprefix $(DESTDIR)/$(PREFIX)/share/pkg-perl-autopkgtest/heavy-deps.d/, $(AUTOPKGTEST_SCRIPTS_HEAVYDEPS))
INSTALLED_ZSH_COMPLETIONS:= $(addprefix $(DESTDIR)/$(PREFIX)/share/zsh/vendor-completions/, $(ZSH_COMPLETIONS))
INSTALLED_CONF_MANUALS := $(addprefix $(DESTDIR)/$(MAN5_DIR)/,$(addsuffix .5,$(CONF_MANUALS)))

INSTALL := install -D -m 0644
INSTALL_BIN := install -D -m 0755
POD2MAN := pod2man --release="pkg-perl-tools $(VERSION)" -c "Debian Perl packaging Tools"

.PHONY: install
install: $(INSTALLED_SCRIPTS) $(INSTALLED_SCRIPT_MANUALS) \
    $(INSTALLED_SCRIPT_ALIAS_MANUALS) \
    $(INSTALLED_SCRIPT_LIBS) \
    $(INSTALLED_BINS) $(INSTALLED_BIN_MANUALS) \
    $(INSTALLED_LIBS) $(INSTALLED_LIB_MANUALS) \
    $(INSTALLED_CONF_MANUALS) \
    $(INSTALLED_LINTIAN_CHECKS) $(INSTALLED_LINTIAN_PROFILES) $(INSTALLED_LINTIAN_TAGS) \
    $(INSTALLED_AUTOPKGTEST_SCRIPTS) \
    $(INSTALLED_AUTOPKGTEST_SCRIPTS_BUILDDEPS) \
    $(INSTALLED_AUTOPKGTEST_SCRIPTS_RUNTIMEDEPS) \
    $(INSTALLED_AUTOPKGTEST_SCRIPTS_RUNTIMEDEPSRECOMMENDS) \
    $(INSTALLED_AUTOPKGTEST_SCRIPTS_HEAVYDEPS) \
    $(INSTALLED_ZSH_COMPLETIONS)

$(DESTDIR)/$(MAN_DIR) \
$(DESTDIR)/$(MAN3_DIR) \
$(DESTDIR)/$(MAN5_DIR) \
$(DESTDIR)/$(PREFIX)/share/perl5/Debian/PkgPerl \
$(DESTDIR)/$(PREFIX)/share/pkg-perl-tools/lib \
$(DESTDIR)/$(PREFIX)/share/lintian/lib/Lintian/Check/PkgPerl \
$(DESTDIR)/$(PREFIX)/share/lintian/profiles/pkg-perl \
$(DESTDIR)/$(PREFIX)/share/lintian/tags/pkg-perl \
$(DESTDIR)/$(PREFIX)/share/pkg-perl-autopkgtest \
$(DESTDIR)/$(PREFIX)/share/pkg-perl-autopkgtest/build-deps.d \
$(DESTDIR)/$(PREFIX)/share/pkg-perl-autopkgtest/runtime-deps.d \
$(DESTDIR)/$(PREFIX)/share/pkg-perl-autopkgtest/runtime-deps-and-recommends.d \
$(DESTDIR)/$(PREFIX)/share/pkg-perl-autopkgtest/heavy-deps.d \
$(DESTDIR)/$(PREFIX)/share/zsh/vendor-completions :
	$(INSTALL) -d -m 0755 $@

$(INSTALLED_SCRIPTS): $(DESTDIR)/$(PREFIX)/share/pkg-perl-tools/% : scripts/%
	$(INSTALL_BIN) $< $@

$(DESTDIR)/$(MAN_DIR)/dpt-co.1: $(DESTDIR)/$(MAN_DIR)/dpt-checkout.1
	ln -s dpt-checkout.1 $@

$(INSTALLED_SCRIPT_MANUALS) : $(DESTDIR)/$(MAN_DIR)/dpt-%.1 : scripts/% $(DESTDIR)/$(MAN_DIR)
	$(POD2MAN) -n "dpt-$*" $< > $@

$(INSTALLED_SCRIPT_LIBS): $(DESTDIR)/$(PREFIX)/share/pkg-perl-tools/lib/% : scripts/lib/% $(DESTDIR)/$(PREFIX)/share/pkg-perl-tools/lib
	$(INSTALL) $< $@

$(INSTALLED_BINS) : $(DESTDIR)/$(PREFIX)/bin/% : bin/%
	$(INSTALL_BIN) $< $@
	sed -i 's,DPT__SCRIPTS=scripts # REPLACED.*,DPT__SCRIPTS="/$(PREFIX)/share/pkg-perl-tools",' $@

$(INSTALLED_BIN_MANUALS) : $(DESTDIR)/$(MAN_DIR)/%.1 : bin/% $(DESTDIR)/$(MAN_DIR)
	$(POD2MAN) -n "$*" $< > $@

$(INSTALLED_LIBS) : $(DESTDIR)/$(PREFIX)/share/perl5/Debian/PkgPerl/% : lib/Debian/PkgPerl/% $(DESTDIR)/$(PREFIX)/share/perl5/Debian/PkgPerl
	$(INSTALL_BIN) $< $@

# Using // instead of :: helps finding the original .pm file
.PHONY: $(INSTALLED_LIB_MANUALS)
$(INSTALLED_LIB_MANUALS) : $(DESTDIR)/$(MAN3_DIR)/%.3pm : lib/%.pm $(DESTDIR)/$(MAN3_DIR)
	$(POD2MAN) -n "$(subst //,::,$*)" -s 3pm $< > $(subst //,::,$@)

$(INSTALLED_LINTIAN_CHECKS) : $(DESTDIR)/$(PREFIX)/share/lintian/lib/Lintian/Check/PkgPerl/% : lintian/checks/PkgPerl/% $(DESTDIR)/$(PREFIX)/share/lintian/lib/Lintian/Check/PkgPerl
	$(INSTALL) $< $@

$(INSTALLED_LINTIAN_PROFILES) : $(DESTDIR)/$(PREFIX)/share/lintian/profiles/pkg-perl/% : lintian/profiles/pkg-perl/% $(DESTDIR)/$(PREFIX)/share/lintian/profiles/pkg-perl
	$(INSTALL) $< $@

$(INSTALLED_LINTIAN_TAGS) : $(DESTDIR)/$(PREFIX)/share/lintian/tags/pkg-perl/% : lintian/tags/pkg-perl/% $(DESTDIR)/$(PREFIX)/share/lintian/tags/pkg-perl
	$(INSTALL) $< $@

$(INSTALLED_AUTOPKGTEST_SCRIPTS) : $(DESTDIR)/$(PREFIX)/share/pkg-perl-autopkgtest/% : autopkgtest/scripts/% $(DESTDIR)/$(PREFIX)/share/pkg-perl-autopkgtest
	$(INSTALL) $< $@

$(INSTALLED_AUTOPKGTEST_SCRIPTS_BUILDDEPS) : $(DESTDIR)/$(PREFIX)/share/pkg-perl-autopkgtest/build-deps.d/% : autopkgtest/scripts/build-deps.d/% $(DESTDIR)/$(PREFIX)/share/pkg-perl-autopkgtest/build-deps.d
	$(INSTALL) $< $@

$(INSTALLED_AUTOPKGTEST_SCRIPTS_RUNTIMEDEPS) : $(DESTDIR)/$(PREFIX)/share/pkg-perl-autopkgtest/runtime-deps.d/% : autopkgtest/scripts/runtime-deps.d/% $(DESTDIR)/$(PREFIX)/share/pkg-perl-autopkgtest/runtime-deps.d
	$(INSTALL) $< $@

$(INSTALLED_AUTOPKGTEST_SCRIPTS_RUNTIMEDEPSRECOMMENDS) : $(DESTDIR)/$(PREFIX)/share/pkg-perl-autopkgtest/runtime-deps-and-recommends.d/% : autopkgtest/scripts/runtime-deps-and-recommends.d/% $(DESTDIR)/$(PREFIX)/share/pkg-perl-autopkgtest/runtime-deps-and-recommends.d
	$(INSTALL) $< $@

$(INSTALLED_AUTOPKGTEST_SCRIPTS_HEAVYDEPS) : $(DESTDIR)/$(PREFIX)/share/pkg-perl-autopkgtest/heavy-deps.d/% : autopkgtest/scripts/heavy-deps.d/% $(DESTDIR)/$(PREFIX)/share/pkg-perl-autopkgtest/heavy-deps.d
	$(INSTALL) $< $@

$(INSTALLED_ZSH_COMPLETIONS) : $(DESTDIR)/$(PREFIX)/share/zsh/vendor-completions/% : shell/zsh/% $(DESTDIR)/$(PREFIX)/share/zsh/vendor-completions
	$(INSTALL) $< $@

$(INSTALLED_CONF_MANUALS) : $(DESTDIR)/$(MAN5_DIR)/%.5 : man5/%.pod $(DESTDIR)/$(MAN5_DIR)
	$(POD2MAN) -n "$*" --section 5 $< > $@

.PHONY: clean
clean:

test:
	for f in `find bin scripts -maxdepth 1 -type f`; do \
	    if head -1 $$f | grep -q bin/perl; then \
		perl -Ilib -c $$f || exit 1; \
	    elif head -1 $$f |grep -q python; then \
		python3 -c "compile(open('$$f').read(), '', 'exec')" && echo "$$f python script OK" || exit 1; \
	    elif head -1 $$f |grep -q bash; then \
		bash -n $$f && echo "$$f bash script OK" || exit 1; \
	    else \
		sh -n $$f && echo "$$f shell script OK" || exit 1; \
	    fi \
	done
	for f in `find . -name '*.pm' -type f`; do \
	    perl -Ilib -c $$f || exit 1; \
	done
	prove -l --verbose --recurse t/
	-prove -l --verbose --recurse --ext .st t/
