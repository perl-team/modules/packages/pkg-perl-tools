use Test::More;
use Test::Pod;

my @pod_dirs = qw( bin scripts man5 );

all_pod_files_ok( map( glob("$_/*"), @pod_dirs ) );
