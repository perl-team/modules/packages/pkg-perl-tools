#!/usr/bin/perl

use strict;
use warnings;

use Test::More;

use File::Basename qw( dirname );

BEGIN {
    plan skip_all
        => "Set AUTHOR_TESTING to perform these tests"
        unless $ENV{AUTHOR_TESTING};

    plan skip_all
        => "These tests require a working Debian::PkgPerl::Message"
        unless eval { use Debian::PkgPerl::Message; 1 };
}

# Defaults
my %params = (
    interactive => 0,
    dist        => 'pkg-perl-dummy',
    name        => 'DPT Tester',
    email       => 'test@example.com',
    mailto      => 'upstream@example.com',
);

my $patch = dirname(__FILE__) . '/dummy.txt.patch';

subtest 'Test bug message' => sub {
    plan skip_all
        => "Bug tests require a working Debian::PkgPerl::Bug"
         . " and LWP::Simple"
        unless eval { use Debian::PkgPerl::Bug; 1 }
        and eval { use LWP::Simple; 1 };

    use Test::RequiresInternet ( 'bugs.debian.org' => 80 );
    use Test::RequiresInternet ( 'bugs.debian.org' => 443 );

    my $html = get('https://bugs.debian.org/src:pkg-perl-tools');
    my @list = $html =~ /<a href="bugreport\.cgi\?bug=(\d+)">#\1<\/a>/g;
    my $bug = $list[0];

    my %info = Debian::PkgPerl::Bug->new( bug => $bug )
        ->retrieve_bug_info();

    my $msg = new_ok( 'Debian::PkgPerl::Message', [
        %params,
        info    => \%info,
        tracker => 'github',
    ]);

    like( $msg->get_subject(), qr/$info{Subject}/, 'bug subject' );
    like( $msg->prepare_body(), qr/bugs\.debian\.org\/$bug/m, 'bug body' );

    done_testing();
};

subtest 'Test patch message to GitHub' => sub {
    plan skip_all
        => "Patch tests require a working Debian::PkgPerl::Patch"
        unless eval { require Debian::PkgPerl::Patch; 1 };

    my %info = Debian::PkgPerl::Patch->new( patch => $patch )
        ->retrieve_patch_info();

    my $msg = new_ok( 'Debian::PkgPerl::Message', [
        %params,
        info    => \%info,
        tracker => 'github',
        url     => 'https://github.com/alexm/pkg-perl-dummy/issues',
    ]);

    like( $msg->get_subject(), qr/$info{Subject}/, 'patch subject' );
    unlike( $msg->prepare_body(), qr/^(Description|Author)/m, 'patch body' );
    like(
        $msg->prepare_body(),
        qr{^https://salsa\.debian\.org/perl-team/modules/packages/pkg-perl-tools/raw/master/t/dummy\.txt\.patch$}m,
        'patch URL'
    );

    done_testing();
};


subtest 'Test patch message to CPAN' => sub {
    plan skip_all
        => "Patch tests require a working Debian::PkgPerl::Patch"
        unless eval { require Debian::PkgPerl::Patch; 1 };

    my %info = Debian::PkgPerl::Patch->new( patch => $patch )
        ->retrieve_patch_info();

    my $msg = new_ok( 'Debian::PkgPerl::Message', [
        %params,
        info    => \%info,
        tracker => 'cpan',
        url     => 'https://rt.cpan.org/Public/Dist/Display.html?Name=DUMMY',
    ]);

    like( $msg->get_subject(), qr/$info{Subject}/, 'patch subject' );
    like( $msg->prepare_body(), qr/^(Description|Author)/m, 'patch body' );
    like(
        $msg->prepare_body(),
        qr{^https://salsa\.debian\.org/perl-team/modules/packages/pkg-perl-tools/raw/master/t/dummy\.txt\.patch$}m,
        'patch URL'
    );

    done_testing();
};

done_testing();
