#!/bin/sh

# copyright, licensing and documentation at the end

. "${DPT__SCRIPTS:-/usr/share/pkg-perl-tools}/lib/dpt-lib.sh"

is_key_pkg () {
	is_command curl
	local P="$1"
	local RES
	RES=$(echo "$P" | curl --silent --data-binary @- \
		https://udd.debian.org/cgi-bin/select-key-packages.cgi)
	if [ "$RES" ]
	then
		echo $(colored "GREEN" "✔") "$P is a key package"
	else
		echo $(colored "RED" "✘") "$P is no key package"
	fi
}

ci_report () {
	psql --quiet "$UDD_PG" <<EOF | sed "s/\\bpass\\b/$(colored GREEN '  ✔ ')/; s/\\bfail\\b/$(colored RED '  ✘ ')/;" | head -n -1
\\pset border 1
\\pset footer off
select distinct on (suite)
       suite, date, version, status
from ci
where source='$1'
order by suite, date desc;
EOF
}

header "gbp pull"
gbp pull

header "Bugs"
if is_installed "pkg-perl-tools" "0.69"; then
	bts-srcpkg -t
else
	warn "Running 'bts-srcpkg' without -t, sorry for the duplicate title."
	bts-srcpkg
fi

header "rmadison"
rmadison $PKG

header "grep-excuses"
EXCUSES=$(grep-excuses --wipnity $PKG)
if [ -z "$EXCUSES" ] || [ "$EXCUSES" = "No excuse for $PKG" ]; then
	colored "GREEN" "None \o/"
else
	colored "YELLOW" "$EXCUSES"
fi

header "Last upload"
who-uploads --date --max-uploads=1 $PKG

header "Popcon"
if is_command popcon python3-popcon; then
	if is_installed "python3-popcon" "3.0.0"; then
		popcon --verbose $PKG
	else
		info "Output is nicer with python3-popcon >= 3.0.0."
		popcon $PKG
	fi
fi

header "Key package?"
is_key_pkg $PKG

header "CI (amd64)"
if is_command psql postgresql-client; then
	ci_report $PKG
fi

header "New upstream release?"
USCAN=$(uscan --report)
if [ -n "$USCAN" ]; then
	colored "YELLOW" "$USCAN"
else
	colored "GREEN" "None \o/"
fi

header "Git diff against last Debian tag"
gitddiff

POD=<<'EOF'
=head1 NAME

dpt-prepare - prepare yourself and a source directory before working on a package

=head1 SYNOPSIS

B<dpt prepare>

=head1 DESCRIPTION

B<dpt prepare> runs a couple of commands in the checked out directory of a
source package which are useful -- either for the person or the code -- as a
preparation before working on the package.

=head1 CONFIGURATION, OPTIONS, & PARAMETERS

None (yet).

=head1 COPYRIGHT & LICENSE

=over

=item Copyright 2022-2024 gregor herrmann L<gregoa@debian.org>

=item Copyright 2022 Damyan Ivanov L<dmn@debian.org>

=back

This program is free software, licensed under the same term as perl.

=cut
EOF
