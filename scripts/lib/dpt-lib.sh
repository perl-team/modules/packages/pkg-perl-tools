# collection of shell function and variables  for dpt-* scripts

# to be sourced from dpt-* scripts as
# . "${DPT__SCRIPTS:-/usr/share/pkg-perl-tools}/lib/dpt-lib.sh"

# Copyright 2022, gregor herrmann <gregoa@debian.org>
# Copyright 2022, Damyan Ivanov <dmn@debian.org>
# Released under the same terms as perl

# documentation in (git) man5/dpt-lib.sh.pod /
# (installed) man 5 dpt-lib.sh

# COLO(U)RS

C_BLACK=$(tput setaf 0)
C_RED=$(tput setaf 1)
C_GREEN=$(tput setaf 2)
C_YELLOW=$(tput setaf 3)
C_BLUE=$(tput setaf 4)
C_MAGENTA=$(tput setaf 5)
C_CYAN=$(tput setaf 6)
C_WHITE=$(tput setaf 7)
BG_C_BLACK=$(tput setab 0)
BG_C_RED=$(tput setab 1)
BG_C_GREEN=$(tput setab 2)
BG_C_YELLOW=$(tput setab 3)
BG_C_BLUE=$(tput setab 4)
BG_C_MAGENTA=$(tput setab 5)
BG_C_CYAN=$(tput setab 6)
BG_C_WHITE=$(tput setab 7)
C_RESET=$(tput sgr0)

[ ! -t 1 -o ! -t 2 ] && NO_COLOR=1

# FUNCTIONS

colored() {
	local COLORNAME=$1
	shift
	local MSG=$*

	if [ -n "${NO_COLOR:-}" ]; then
		echo "$MSG"
		return
	fi

	# https://unix.stackexchange.com/questions/557361/how-to-dynamically-set-a-variable-name-in-shell-script
	VAR=C_$COLORNAME
	C_SET=$(eval "echo \"\$$VAR\"")

	echo "${C_SET}${MSG}${C_RESET}"
}

extra_colored() {
	local COLORNAME="$1"
	local BG_COLORNAME="$2"
	shift 2
	local MSG=$*

	if [ -n "${NO_COLOR:-}" ]; then
		echo "$MSG"
		return
	fi

	# https://unix.stackexchange.com/questions/557361/how-to-dynamically-set-a-variable-name-in-shell-script
	local VAR="C_$COLORNAME"
	local BG_VAR="BG_C_$BG_COLORNAME"
	local C_SET=$(eval "echo \"\$$VAR\$$BG_VAR\"")

	echo "${C_SET}${MSG}${C_RESET}"
}

die() {
	colored "RED" "E: $*" >&2
	exit 1
}

warn() {
	colored "YELLOW" "W: $*" >&2
}

info() {
	colored "GREEN" "I: $*" >&2
}

header() {
	local TITLE="$*"
	local LEN=$(( $(echo "$TITLE" | perl -pe 's/\e\[?.*?[\@-~]//g' | wc --chars) - 1 )) # or sed -e 's/\x1b\[[0-9;]*m//g'
	local UNDER=""
	echo
	colored "CYAN" "$TITLE"
	for i in $(seq $LEN); do
		UNDER="$UNDER="
	done
	colored "CYAN" "$UNDER"
}

anykey () {
	if [ $SHELL = "/bin/bash" ]; then
		bash -c 'read -n 1 -p "Press the anykey ..."'
	elif [ $SHELL = "/bin/zsh" ]; then
		zsh -c 'read -k 1 "?Press the anykey ..."'
	else
		read -p "Press enter ..." DUMMY
	fi
}

# bash specific
promptyesno () {
	local PROMPT="$*"
	read -n 1 -p "$(extra_colored "WHITE" "BLUE" $PROMPT [y/N]) " ANSWER
	case $ANSWER in
		y|Y) return 0 ;;
		*)   return 1 ;;
	esac
}

is_installed() {
	local PKG=$1
	local VER=${2:-''}

	local D=$(dpkg-query --show $PKG 2>/dev/null)
	local D_PKG=$(echo $D | awk '{print $1}')
	local D_VER=$(echo $D | awk '{print $2}')

	if [ -z "$D_PKG" ]; then
		warn "Package '$PKG' is not installed."
		return 1
	fi
	if [ -n "VER" ] && dpkg --compare-versions $D_VER lt $VER ; then
		warn "Version '$D_VER' of package '$PKG' does not satisfy version requirement '$VER'."
		return 1
	fi
}

is_command() {
	local CMD=$1
	local PKG=${2:-''}
	if ! command -v $CMD >/dev/null; then
		warn "Can't find command '$CMD'."
		if [ -n "$PKG" ]; then
			warn "You might want to install the '$PKG' package."
		fi
		return 1
	fi
}

is_pkg_perl() {
	grep -qF 'pkg-perl-maintainers@lists.alioth.debian.org' debian/control
}

gitddiff() {
	local TAG
	TAG=$(git describe --abbrev=0 --match "$(gbp config DEFAULT.debian-tag | sed -e 's/%(version)s/*/g;')" --tags 2>/dev/null || true)

	if [ $( gitdiff $TAG|head -1|wc -l ) = "0" ]; then
		colored "GREEN" "None \o/"
	else
		anykey
		git diff --ignore-space-at-eol $TAG
	fi
}

OOT=${OOT:-''}
if [ -z "$OOT" ]; then
# sanity checks
	dh_testdir  || die "This doesn't look like a source package directory: $(pwd)"
	[ -d .git ] || die "No .git directory found: $(pwd)"

# VARIABLES
	PKG=$(dpkg-parsechangelog --show-field Source)
	VER=$(dpkg-parsechangelog --show-field Version)
	UVER=$(echo "$VER" | sed 's,^[0-9]\+:,,; s,-[^-]\+$,,')
	DIST=$(dpkg-parsechangelog --show-field Distribution)
fi
UDD_PG='postgresql://udd-mirror:udd-mirror@udd-mirror.debian.net/udd'
