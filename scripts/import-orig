#!/bin/bash

# copyright, licensing and documentation at the end

set -e
set -u

. "${DPT__SCRIPTS:-/usr/share/pkg-perl-tools}/lib/dpt-lib.sh"

# defaults for configuration variable(s)

DPT_RM_TARGZ=${DPT_RM_TARGZ:-''}

# origin upstream-repo
header "Git remote 'upstream-repo'"
dpt upstream-repo -u

# origin remote
header "gbp pull"
gbp pull

# uscan and versions
USCAN="$(uscan --report-status --dehs 2>/dev/null || true)"
UVERSION="$(echo $USCAN | perl -nE '/<upstream-version>([^<]+)<\/upstream-version>/ and say $1')"
UVERSIONZERO="$(echo "$UVERSION" | perl -ne 'print if s/0+$//')"
# in case of error, don't crash, just set to null string.
UTAG=''
UTAG="$(git tag | grep -x "v\?$UVERSION" || git tag | grep -- "[_-]$UVERSION\$")" || /bin/true
if [ -z "$UTAG" ] && [ -n "$UVERSIONZERO" ]; then
	UTAG="$(git tag | grep -x "v\?$UVERSIONZERO" || git tag | grep -- "[_-]$UVERSIONZERO\$")" || /bin/true
fi
UNAME=$(awk '/Upstream-Name:/ {print $2;}' debian/copyright)

# Download and import.
# We're using --no-interactive here because the dpt() shell function and
# "gbp import-orig" don't play well together (waiting for input before
# showing the question).
# Also create a changelog entry as a start, and to un-confuse git-dch later.
header "gbp import-orig"
gbp import-orig --no-interactive --pristine-tar \
	--filter='.gitignore' --filter='debian/*' --filter='.git/*' --filter='.git' \
	--postimport='dch -v${GBP_DEBIAN_VERSION} --no-auto-nmu --mainttrailer --release-heuristic=changelog "Import upstream version ${GBP_UPSTREAM_VERSION}."' \
	${UTAG:+--upstream-vcs-tag="$UTAG"} ${1:---uscan}

git add debian/changelog
git commit -m "Update debian/changelog" -m "Gbp-Dch: Ignore"

# cleanup on request
if [ -n "$DPT_RM_TARGZ" ] ; then
	rm -fv ../${PKG}*.tar.gz* ../${PKG}*.tgz* ../${PKG}*.tar.xz* ../${PKG}*.tar.bz2* ../${PKG}*.tar.lzma* ../${PKG}*.zip*
	rm -fv ../${UNAME}*.tar.gz ../${UNAME}*.tgz ../${UNAME}*.tar.xz ../${UNAME}*.tar.bz2 ../${UNAME}*.tar.lzma ../${UNAME}*.zip
fi

# diff against last debian/ tag
header "Git diff against last Debian tag"
gitddiff

# diff between upstream-repo tag and upstream/ (tarball) tag
[ -n "$UTAG" ] || exit 0
TARTAG=$(git describe --abbrev=0 --match "$(gbp config DEFAULT.upstream-tag | sed -e 's/%(version)s/*/g;')")
echo
header "Compare Git tags"
if promptyesno "View diff between upstream Git tag ('$UTAG') and imported tarball ('$TARTAG')? y/N"; then
	git diff --ignore-space-at-eol "$UTAG" "$TARTAG"
fi

POD=<<'EOF'
=head1 NAME

dpt-import-orig - "gbp import-orig" wrapper with upstream tracking support

=head1 SYNOPSIS

B<dpt import-orig> [I<../package_version.orig.tar.gz>]

=head1 DESCRIPTION

B<dpt import-orig> is a wrapper around B<gbp import-orig> using its
B<--uscan> and B<--upstream-vcs-tag> options to fetch and import tarballs
and link to upstream Git tags.

If necessary, the B<git remote> is added using L<dpt-upstream-repo(1)>.

If a parameter is passed, it's taken as orig tar-ball to import
instead of using uscan to download the tar ball. Needed if a newer
upstream release has a (from dpkg's point of view) lower version
number and gbp bails out over uscan not having downloaded a tar ball.

=head1 CONFIGURATION

B<dpt import-orig> reads the C<DPT_RM_TARGZ> environment variable to
determine if the downloaded F<.orig.tar.gz> should be removed after the
import.

See L<dpt-config(5)> for details.

=head1 SEE ALSO

L<dpt-upstream-repo(1)>

=head1 COPYRIGHT & LICENSE

=over

=item Copyright 2013 Jonas Smedegaard L<dr@jones.dk>

=item Copyright 2013-2024 gregor herrmann L<gregoa@debian.org>

=item Copyright 2014 David Bremner L<bremner@debian.org>

=back

This program is free software, licensed under the same term as perl.

=cut
EOF
